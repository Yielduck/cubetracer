#include "gui.h"
#include <GLFW/glfw3.h>
#include <imgui/imgui.h>
GUI::GUI(ImguiState state)
    : imguiState(static_cast<ImguiState &&>(state))
{
    int iw = 0, ih = 0;
    glfwGetWindowSize(state.window, &iw, &ih);
    camera =
    {
        .phi = 0.f,
        .theta = 0.f,
        .ratio = static_cast<float>(iw)
               / static_cast<float>(ih),
        .tanFOV = 1.f,
        .pos = {0.f, 0.f, 0.f},
    };

    ImGui::StyleColorsDark();
    glfwSetWindowUserPointer(state.window, this);

    glfwSetKeyCallback(state.window, +[](GLFWwindow * const window, int key, int, int action, int) noexcept
    {
        if(action == GLFW_RELEASE)
            return;

        GUI &gui = *reinterpret_cast<GUI *>(glfwGetWindowUserPointer(window));
        if(key == GLFW_KEY_ESCAPE)
            gui.show = !gui.show;
        if(gui.show)
            return;

        Camera &cam = gui.camera;
        auto const [x, y, z] = cam.rotate();
        vec3 const speed = toVec3(gui.cameraSpeed);
        switch(key)
        {
            case GLFW_KEY_W:        cam.pos += z * speed; break;
            case GLFW_KEY_S:        cam.pos -= z * speed; break;
            case GLFW_KEY_D:        cam.pos += x * speed; break;
            case GLFW_KEY_A:        cam.pos -= x * speed; break;
            case GLFW_KEY_SPACE:    cam.pos += y * speed; break;
            case GLFW_KEY_C:        cam.pos -= y * speed; break;
            default: break;
        }
    });
    glfwGetCursorPos(state.window, &mouseX, &mouseY);
    glfwSetCursorPosCallback(state.window, +[](GLFWwindow * const window, double const x, double const y) noexcept
    {
        GUI &gui = *reinterpret_cast<GUI *>(glfwGetWindowUserPointer(window));
        if(gui.show)
            return;

        gui.camera.phi   += gui.cameraSensivity * static_cast<float>(x - gui.mouseX);
        gui.camera.theta -= gui.cameraSensivity * static_cast<float>(y - gui.mouseY);
        gui.camera.theta = std::min(std::max(gui.camera.theta, -1.5707f), 1.5707f);

        gui.mouseX = x;
        gui.mouseY = y;
    });
}
void GUI::render(vk::guard::RenderPass const &pass)
{
    int width, height;
    glfwGetWindowSize(imguiState, &width, &height);

    glfwSetInputMode(imguiState, GLFW_CURSOR, show ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);

    ImGuiIO &io = ImGui::GetIO();

    ImGui_ImplVulkan_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    if(show)
    {
        float const w = 300.f;
        ImGui::SetNextWindowPos(ImVec2(static_cast<float>(width) - w, 0.f), ImGuiCond_FirstUseEver);
        ImGui::SetNextWindowSize(ImVec2(w, 88.f), ImGuiCond_FirstUseEver);
        ImGui::Begin("Camera", &show);
        ImGui::SliderFloat("Sensivity", &cameraSensivity, 1e-3f, 1e-2f, "%.4f");
        ImGui::SliderFloat("Speed", &cameraSpeed, 1e-1f, 1e1f, "%.2f", ImGuiSliderFlags_Logarithmic);
        ImGui::End();
    }
    {
        ImGui::SetNextWindowPos(ImVec2(0.f, 0.f));
        ImGui::SetNextWindowSize(ImVec2(160.f, 100.f));
        ImGui::Begin("Info", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);
        if(ImGui::Button("Quit"))
            glfwSetWindowShouldClose(imguiState, GLFW_TRUE);
        ImGui::Text("%.2f ms, %.1f FPS", 1000.0f / io.Framerate, io.Framerate);
        ImGui::Text("ESC to toggle GUI focus");
        ImGui::End();
    }

    ImGui::Render();
    ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), pass.buffer);
}

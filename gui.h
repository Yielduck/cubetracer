#pragma once
#include "camera.h"
#include "imgui_state.h"
#include <vk/guard/render_pass.h>

class GUI
{
    vk::StoringGuard<ImguiState> imguiState;
    double mouseX, mouseY;

public:
    Camera camera;

    bool show = false;

    float cameraSpeed = 1.f;
    float cameraSensivity = 5e-3f;

    GUI(ImguiState);
    void render(vk::guard::RenderPass const &);
};

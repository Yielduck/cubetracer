#pragma once
#include "camera.h"
#include <vk/buffer.h>
#include <vk/device_memory.h>
#include <vk/image.h>
#include <vk/image_view.h>
#include <vk/command_buffer.h>
#include <vk/descriptor_set_layout.h>
#include <vk/descriptor_set.h>
#include <vk/graphics_pipeline.h>
#include <vk/guard/render_pass.h>
#include <vk/pipeline_layout.h>
#include <vk/sampler.h>

struct GPU;
struct World;

class Renderer
{
    VkDevice                    device;
    VkQueue                     transferQueue;

    vk::Sampler                 sampler;
    vk::Image                   image[2];
    vk::ImageView               imageView[2];
    vk::DeviceMemory            imageMemory[2];
    VkImageLayout               imageLayout[2];
    static constexpr VkExtent3D imageExtent = {576, 256, 576};
    static constexpr vk::u32    voxels = imageExtent.width
                                       * imageExtent.height
                                       * imageExtent.depth;

    vk::Buffer                  stagingBuffer;
    vk::DeviceMemory            stagingMemory;
    vk::CommandBuffer           stagingCommandBuffer;

    vk::DescriptorSetLayout     descriptorSetLayout;
    vk::DescriptorSet           descriptorSet;

    struct Push
    {
        vk::f32 cam[12];
    };
    vk::PipelineLayout          graphicsPipelineLayout;
    vk::GraphicsPipeline        graphicsPipeline;

public:
    Renderer(GPU const &);
    void initPipeline(VkRenderPass);
    void update(World const &);

    struct RenderData
    {
        Camera camera;
    };
    void render(vk::guard::RenderPass const &, RenderData const &) noexcept;
};

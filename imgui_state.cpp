#include "imgui_state.h"
#include <vk/guard/command_buffer.h>
#include <vk/queue_submit.h>
ImguiState::value_t ImguiState::acquire() const
{
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGui_ImplGlfw_InitForVulkan(window, true);
    ImGui_ImplVulkan_Init(const_cast<ImGui_ImplVulkan_InitInfo *>(&info), renderPass); // thank you imgui for your const-correctness

    ImGuiIO &io = ImGui::GetIO();
    io.FontGlobalScale = 1.f / fontScale;
    io.Fonts->AddFontFromFileTTF(FONT_DIR "Roboto-Medium.ttf", 16.f * fontScale);

    {
        vk::guard::CommandBuffer const guard({commandBuffer, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT});
        ImGui_ImplVulkan_CreateFontsTexture(guard);
    }
    vk::queueSubmit(info.Queue, {{ {.commandBuffer = {{commandBuffer}} } }});
    vkQueueWaitIdle(info.Queue);

    ImGui_ImplVulkan_DestroyFontUploadObjects();

    return window;
}
void ImguiState::release(value_t) const
{
    ImGui_ImplVulkan_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

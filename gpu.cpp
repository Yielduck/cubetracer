#include "gpu.h"
#include <vk/command_pool.h>
#include <vk/device.h>
#include <vk/descriptor_pool.h>
#include <vk/instance.h>
#include <algorithm>
#include <stdexcept>
#include <cassert>

GPU::GPU(vk::cspan<char const *> const instanceExt)
{
    instance = vk::createInstance
    ({
        .apiVersion = VK_API_VERSION_1_1,
#ifdef VALIDATION_LAYER
        .validationLayer = {{VALIDATION_LAYER}},
#endif
        .extension = instanceExt,
    });

    auto const physicalDeviceInfo = vk::physicalDeviceInfo(instance);
    if(std::ranges::empty(physicalDeviceInfo))
        throw std::runtime_error("no physical device found");

    auto const discreteGPUIter = std::ranges::find
    (
        physicalDeviceInfo,
        VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU,
        +[](vk::PhysicalDeviceInfo const &info) noexcept {return info.properties.deviceType;}
    );
    deviceInfo = discreteGPUIter == physicalDeviceInfo.end()
        ? physicalDeviceInfo[0]
        : *discreteGPUIter;

    auto gqueue = deviceInfo.findQueueFamily(VK_QUEUE_GRAPHICS_BIT);
    auto cqueue = deviceInfo.findQueueFamily(VK_QUEUE_COMPUTE_BIT);
    auto tqueue = deviceInfo.findQueueFamily(VK_QUEUE_TRANSFER_BIT);
    auto equeue = deviceInfo.findQueueFamily(VK_QUEUE_GRAPHICS_BIT
                                           | VK_QUEUE_COMPUTE_BIT
                                           | VK_QUEUE_TRANSFER_BIT);

    if(std::ranges::empty(gqueue))
        throw std::runtime_error("selected device has no graphics queue");
    if(std::ranges::empty(cqueue))
        throw std::runtime_error("selected device has no  compute queue");
    if(std::ranges::empty(tqueue))
        throw std::runtime_error("selected device has no transfer queue");

    auto computeOnlyQueue = cqueue | std::views::filter
    (
        [&](vk::u32 const family) noexcept
        {
            return gqueue.end() == std::ranges::find(gqueue, family)
                && tqueue.end() == std::ranges::find(tqueue, family);
        }
    );
    auto transferOnlyQueue = tqueue | std::views::filter
    (
        [&](vk::u32 const family) noexcept
        {
            return gqueue.end() == std::ranges::find(gqueue, family)
                && cqueue.end() == std::ranges::find(cqueue, family);
        }
    );

    vk::cspan<vk::DeviceCreateInfo::QueueInfo> queueInfoSpan;
    vk::DeviceCreateInfo::QueueInfo queueInfo[3];
    float const priority[3] = {1.f, 0.f, 0.5f};

    if(!std::ranges::empty(           gqueue) &&
       !std::ranges::empty( computeOnlyQueue) &&
       !std::ranges::empty(transferOnlyQueue))
    {
        queueInfo[0].queueFamilyIndex =            gqueue.front();
        queueInfo[1].queueFamilyIndex =  computeOnlyQueue.front();
        queueInfo[2].queueFamilyIndex = transferOnlyQueue.front();
        for(vk::u32 const i : std::views::iota(0u, 3u))
            queueInfo[i].priority = {priority + i, 1};
        queueInfoSpan = {queueInfo};

        graphicsQueue.family = queueInfo[0].queueFamilyIndex;
         computeQueue.family = queueInfo[1].queueFamilyIndex;
        transferQueue.family = queueInfo[2].queueFamilyIndex;
    }
    else if(!std::ranges::empty(equeue | std::views::drop(2)))
    {
        queueInfo[0].queueFamilyIndex = equeue.front();
        queueInfo[1].queueFamilyIndex = (equeue | std::views::drop(1)).front();
        queueInfo[2].queueFamilyIndex = (equeue | std::views::drop(2)).front();
        for(vk::u32 const i : std::views::iota(0u, 3u))
            queueInfo[i].priority = {priority + i, 1};
        queueInfoSpan = {queueInfo};

        graphicsQueue.family = queueInfo[0].queueFamilyIndex;
         computeQueue.family = queueInfo[1].queueFamilyIndex;
        transferQueue.family = queueInfo[2].queueFamilyIndex;

    }
    else if(!std::ranges::empty(equeue))
    {
        queueInfo[0].queueFamilyIndex = equeue.front();
        queueInfo[0].priority = {priority, 1};
        queueInfoSpan = {queueInfo, 1};

        graphicsQueue.family = queueInfo[0].queueFamilyIndex;
         computeQueue.family = queueInfo[0].queueFamilyIndex;
        transferQueue.family = queueInfo[0].queueFamilyIndex;
    }
    else
        throw std::runtime_error("selected device has no g/c/t queue");

    static constexpr char const * extension[] =
    {
        "VK_KHR_swapchain",
    };
    static constexpr char const * optExtension[] =
    {
        "VK_KHR_8bit_storage",
        "VK_KHR_portability_subset",
#ifdef TRACY_ENABLE
        "VK_EXT_calibrated_timestamps",
        "VK_KHR_pipeline_executable_properties",
        //"VK_KHR_performance_query",
#endif
    };
    char const *enabledExt[16];
    vk::u32 extCount = 0u;
    for(char const * const ext : extension)
    {
        if(false == deviceInfo.supportsExtension(ext))
            throw std::runtime_error("no support for " + std::string(ext));
        enabledExt[extCount++] = ext;
    }
    for(char const * const ext : optExtension)
        if(true == deviceInfo.supportsExtension(ext))
            enabledExt[extCount++] = ext;
    assert(extCount < std::size(enabledExt));

    device = vk::createDevice
    (
        deviceInfo.device,
        {
            .queueInfo = queueInfoSpan,
            .extension = {enabledExt, extCount},
        }
    );

    VkCommandPoolCreateFlags const poolFlags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    graphicsQueue.commandPool = vk::createCommandPool(device, {poolFlags, graphicsQueue.family});
     computeQueue.commandPool = vk::createCommandPool(device, {poolFlags,  computeQueue.family});
    transferQueue.commandPool = vk::createCommandPool(device, {poolFlags, transferQueue.family});

    graphicsQueue.handle = vk::getDeviceQueue(device, graphicsQueue.family);
     computeQueue.handle = vk::getDeviceQueue(device,  computeQueue.family);
    transferQueue.handle = vk::getDeviceQueue(device, transferQueue.family);

    descriptorPool = vk::createDescriptorPool
    (
        device,
        {
            .flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT,
            .maxSets = 8,
            .poolSize =
            {{
                {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 4},
                {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 4},
                {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 4},
            }},
        }
    );

    imguiDescriptorPool = vk::createDescriptorPool
    (
        device,
        {
            .flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT,
            .maxSets = 100,
            .poolSize =
            {{
                {VK_DESCRIPTOR_TYPE_SAMPLER                 , 100},
                {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER  , 100},
                {VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE           , 100},
                {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE           , 100},
                {VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER    , 100},
                {VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER    , 100},
                {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER          , 100},
                {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER          , 100},
                {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC  , 100},
                {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC  , 100},
                {VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT        , 100},
            }},
        }
    );
}

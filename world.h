#pragma once
#include "cube.h"
#include <cmath>

struct World
{
    Cube cube(int x, int y, int z) const noexcept;
};

inline Cube World::cube(int const x, int const y, int const z) const noexcept
{
    float const r2 = static_cast<float>(x * x + z * z);
    return y < 0
        ? Cube::Stone
        : (16 * y * y < std::pow(r2, 0.75) * std::sin(std::sqrt(r2) * 0.2f)
            ? Cube::Grass
            : Cube::Air);
}

#pragma once
#include <imgui/backends/imgui_impl_glfw.h>
#include <imgui/backends/imgui_impl_vulkan.h>

struct ImguiState
{
    GLFWwindow *window;
    ImGui_ImplVulkan_InitInfo info;

    VkRenderPass renderPass;
    VkCommandBuffer commandBuffer;

    float fontScale = 1.f;

    using value_t = GLFWwindow *;
    value_t acquire(       ) const;
    void    release(value_t) const;
};

#pragma once
#include <cstdint>

enum class Cube : std::uint8_t
{
    Air = 0,
    Stone = 1,
    Grass = 2,
};

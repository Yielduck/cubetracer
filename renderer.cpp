#include "camera.h"
#include "gpu.h"
#include "renderer.h"
#include "world.h"
#include <shaders/vs.h>
#include <shaders/fs.h>
#include <vk/everything.h>
#include <ranges>
#include <vector>
#include <tracy/Tracy.hpp>

Renderer::Renderer(GPU const &gpu)
    : device(gpu.device)
    , transferQueue(gpu.transferQueue.handle)
{
    sampler = vk::createSampler(device, {});
    for(vk::u32 const i : std::views::iota(0u, std::size(image)))
    {
        image[i] = vk::createImage
        (
            device,
            {
                .type   = VK_IMAGE_TYPE_3D,
                .format = VK_FORMAT_R8_UINT,
                .extent = imageExtent,
                .usage  = VK_IMAGE_USAGE_SAMPLED_BIT
                        | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
                /*
                .queueFamily =
                {{
                    gpu.graphicsQueue.family,
                    gpu. computeQueue.family,
                    gpu.transferQueue.family,
                }},
                */
            }
        );
        VkMemoryRequirements const req = vk::imageMemoryRequirements(device, image[i]);
        imageMemory[i] = vk::createDeviceMemory
        (
            device,
            {
                .allocationSize = req.size,
                .memoryTypeIndex = gpu.deviceInfo.findMemory(req.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT).front()
            }
        );
        vk::bindImageMemory(device, image[i], imageMemory[i]);
        imageView[i] = vk::createImageView
        (
            device,
            {
                .image = image[i],
                .type = VK_IMAGE_VIEW_TYPE_3D,
                .format = VK_FORMAT_R8_UINT,
            }
        );
        imageLayout[i] = VK_IMAGE_LAYOUT_UNDEFINED;
    }

    descriptorSetLayout = vk::createDescriptorSetLayout
    (
        device,
        {
            .binding =
            {{
                {
                    .binding = 0u,
                    .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                    .descriptorCount = 1u,
                    .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
                    .pImmutableSamplers = nullptr,
                },
                {
                    .binding = 1u,
                    .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                    .descriptorCount = 1u,
                    .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
                    .pImmutableSamplers = nullptr,
                },
            }}
        }
    );
    descriptorSet = vk::createDescriptorSet
    (
        device,
        {
            .descriptorPool = gpu.descriptorPool,
            .setLayout      = descriptorSetLayout,
        }
    );
    vk::updateDescriptorSets
    (
        device,
        {{
            vk::writeDescriptorSet::combinedImageSampler
            (
                descriptorSet,
                0u,
                {{ {
                    .sampler = sampler,
                    .imageView = imageView[0],
                    .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                }} }
            ),
            vk::writeDescriptorSet::combinedImageSampler
            (
                descriptorSet,
                1u,
                {{ {
                    .sampler = sampler,
                    .imageView = imageView[1],
                    .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                }} }
            ),
        }}
    );

    if(sizeof(Push) > gpu.deviceInfo.properties.limits.maxPushConstantsSize)
        throw std::runtime_error("Renderer: push constant range size is too big");
    graphicsPipelineLayout = vk::createPipelineLayout
    (
        device,
        {
            .setLayout = {{descriptorSetLayout}},
            .pushConstantRange =
            {{
                {
                    .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
                    .offset = 0,
                    .size = sizeof(Push),
                }
            }}
        }
    );

    {
        stagingBuffer = vk::createBuffer(device, {.size = voxels, .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT});
        VkMemoryRequirements const req = vk::bufferMemoryRequirements(device, stagingBuffer);
        stagingMemory = vk::createDeviceMemory
        (
            device,
            {
                .allocationSize  = req.size,
                .memoryTypeIndex = gpu.deviceInfo.findMemory(req.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT).front()
            }
        );
        vk::bindBufferMemory(device, stagingBuffer, stagingMemory);

        stagingCommandBuffer = vk::createCommandBuffer
        (
            device,
            {.commandPool = gpu.transferQueue.commandPool}
        );
    }
}

void Renderer::initPipeline(VkRenderPass const renderPass)
{
    vk::ShaderModule const   vertexShader = vk::createShaderModule(device, {vs, std::size(vs)});
    vk::ShaderModule const fragmentShader = vk::createShaderModule(device, {fs, std::size(fs)});

    struct SpecInfo
    {
        vk::u32 ex, ey, ez;
        vk::f32 hitEps;
    };
    graphicsPipeline = vk::createGraphicsPipeline
    (
        device,
        {
            .stage =
            {{
                vk::pipeline::shaderStage
                ({
                    .stage = VK_SHADER_STAGE_VERTEX_BIT,
                    .module = vertexShader,
                }),
                vk::pipeline::shaderStage
                ({
                    .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
                    .module = fragmentShader,
                    .specializationInfo = vk::makeSpecializationInfo
                    (
                        {{
                            {.constantID = 0, .offset = offsetof(SpecInfo,     ex), .size = sizeof(vk::u32)},
                            {.constantID = 1, .offset = offsetof(SpecInfo,     ey), .size = sizeof(vk::u32)},
                            {.constantID = 2, .offset = offsetof(SpecInfo,     ez), .size = sizeof(vk::u32)},
                            {.constantID = 3, .offset = offsetof(SpecInfo, hitEps), .size = sizeof(vk::f32)},
                        }},
                        SpecInfo
                        {
                            .ex = imageExtent.width,
                            .ey = imageExtent.height,
                            .ez = imageExtent.depth,
                            .hitEps = std::numeric_limits<vk::f32>::epsilon() * 2
                                    * std::max(imageExtent.width, std::max(imageExtent.height, imageExtent.depth)),
                        }
                    ),
                }),
            }},
            .vertexInputState   = vk::pipeline::vertexInputState({}),
            .inputAssemblyState = vk::pipeline::inputAssemblyState({.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST}),
            .tessellationState  = vk::pipeline::tessellationState(),
            .viewportState      = vk::pipeline::viewportState({.viewport = {{ {} }}, .scissor = {{ {} }},}),
            .rasterizationState = vk::pipeline::rasterizationState({}),
            .multisampleState   = vk::pipeline::multisampleState({}),
            .depthStencilState  = vk::pipeline::depthStencilState({}),
            .colorBlendState    = vk::pipeline::colorBlendState({}),
            .dynamicState       = vk::pipeline::dynamicState
            ({{
                VK_DYNAMIC_STATE_VIEWPORT,
                VK_DYNAMIC_STATE_SCISSOR,
            }}),
            .layout = graphicsPipelineLayout,
            .renderPass = renderPass,
        }
    );

#ifdef TRACY_ENABLE
    auto const getExecProperties = deviceProcAddr(device, vkGetPipelineExecutablePropertiesKHR);
    auto const getExecStatistics = deviceProcAddr(device, vkGetPipelineExecutableStatisticsKHR);
    if(getExecProperties == nullptr || getExecStatistics == nullptr)
        return;

    VkPipelineInfoKHR const pipelineInfo =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INFO_KHR,
        .pNext = nullptr,
        .pipeline = graphicsPipeline,
    };
    vk::u32 execPropertiesCount = 0u;
    getExecProperties(device, &pipelineInfo, &execPropertiesCount, nullptr);
    std::vector<VkPipelineExecutablePropertiesKHR> execProperties(execPropertiesCount);
    getExecProperties(device, &pipelineInfo, &execPropertiesCount, execProperties.data());
    
    for(vk::u32 const i : std::views::iota(0u, execPropertiesCount))
    {
        std::string const msg = execProperties[i].name
                              + std::string(" (")
                              + execProperties[i].description
                              + std::string("), subgroupSize = ")
                              + std::to_string(execProperties[i].subgroupSize);
        TracyMessage(msg.data(), msg.size());

        VkPipelineExecutableInfoKHR const execInfo =
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INFO_KHR,
            .pNext = nullptr,
            .pipeline = graphicsPipeline,
            .executableIndex = i,
        };
        vk::u32 execStatisticsCount = 0u;
        getExecStatistics(device, &execInfo, &execStatisticsCount, nullptr);
        std::vector<VkPipelineExecutableStatisticKHR> execStatistics(execStatisticsCount);
        getExecStatistics(device, &execInfo, &execStatisticsCount, execStatistics.data());

        for(auto const &stat : execStatistics)
        {
            std::string message = stat.name + std::string(" (") + stat.description + std::string(") = ");
            switch(stat.format)
            {
                case VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_BOOL32_KHR:    message += (stat.value.b32 ? "true" : "false"); break;
                case VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_INT64_KHR:     message += std::to_string(stat.value.i64); break;
                case VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_UINT64_KHR:    message += std::to_string(stat.value.u64); break;
                case VK_PIPELINE_EXECUTABLE_STATISTIC_FORMAT_FLOAT64_KHR:   message += std::to_string(stat.value.f64); break;
                default: assert(0);
            }
            TracyMessage(message.data(), message.size());
        }
    }
#endif
}
void Renderer::update(World const &world)
{
    auto const copyImage = [&, this](vk::u32 const i)
    {
        VkBufferImageCopy const bufferImageCopy =
        {
            .bufferOffset = 0,
            .bufferRowLength = 0,
            .bufferImageHeight = 0,
            .imageSubresource =
            {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .mipLevel = 0,
                .baseArrayLayer = 0,
                .layerCount = 1,
            },
            .imageOffset = {0, 0, 0},
            .imageExtent = imageExtent,
        };
        {
            vk::guard::CommandBuffer const cmdGuard({stagingCommandBuffer, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT});
            vk::cmd::pipelineBarrier
            (
                stagingCommandBuffer,
                {
                    .dstStageMask = VK_PIPELINE_STAGE_TRANSFER_BIT,
                    .imageBarrier =
                    {{ {
                        .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
                        .oldLayout = imageLayout[i],
                        .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                        .image = image[i],
                    }} },
                }
            );
            vk::cmd::copyBufferToImage
            (
                stagingCommandBuffer,
                {
                    .srcBuffer = stagingBuffer,
                    .dstImage = image[i],
                    .region = {{bufferImageCopy}},
                }
            );
            vk::cmd::pipelineBarrier
            (
                stagingCommandBuffer,
                {
                    .srcStageMask = VK_PIPELINE_STAGE_TRANSFER_BIT,
                    .dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                    .imageBarrier =
                    {{ {
                        .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
                        .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
                        .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                        .newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                        .image = image[i],
                    }} },
                }
            );
        }
        vk::queueSubmit
        (
            transferQueue,
            {{ {.commandBuffer      = {{stagingCommandBuffer}} } }}
        );
        imageLayout[i] = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    };

    vk::i32 const dim[3] =
    {
        imageExtent.width,
        imageExtent.height,
        imageExtent.depth,
    };
    vk::i32 const gridMin[3] =
    {
        -dim[0] / 2,
        -dim[1] / 2,
        -dim[2] / 2,
    };

    ZoneScopedNC("cubemap update", 0x4040a0);

    using u8 = unsigned char;
    std::vector<vk::i32> sat(voxels, 0);
    auto const I = [dim](vk::i32 x, vk::i32 y, vk::i32 z) noexcept
    {
        return static_cast<vk::u32>(x + dim[0] * (y + dim[1] * z));
    };

    {
        vk::guard::MemoryMap<Cube> const cube
        ({
            .device = device,
            .memory = stagingMemory,
            .bytes  = voxels,
        });

        for(vk::i32 z = 0; z < dim[2]; ++z)
        for(vk::i32 y = 0; y < dim[1]; ++y)
        for(vk::i32 x = 0; x < dim[0]; ++x)
        {
            vk::u32 const i = I(x, y, z);
            vk::i32 const ix = x + gridMin[0];
            vk::i32 const iy = y + gridMin[1];
            vk::i32 const iz = z + gridMin[2];
            Cube const cubeI = world.cube(ix, iy, iz);
            cube[i] = cubeI;
            sat[i] = cubeI != Cube::Air ? 1 : 0;
        }
    }
    copyImage(0);

    for(vk::i32 z = 0; z < dim[2]; ++z)
    for(vk::i32 y = 0; y < dim[1]; ++y)
    for(vk::i32 x = 0; x < dim[0] - 1; ++x)
        sat[I(x + 1, y, z)] += sat[I(x, y, z)];
    for(vk::i32 z = 0; z < dim[2]; ++z)
    for(vk::i32 y = 0; y < dim[1] - 1; ++y)
    for(vk::i32 x = 0; x < dim[0]; ++x)
        sat[I(x, y + 1, z)] += sat[I(x, y, z)];
    for(vk::i32 z = 0; z < dim[2] - 1; ++z)
    for(vk::i32 y = 0; y < dim[1]; ++y)
    for(vk::i32 x = 0; x < dim[0]; ++x)
        sat[I(x, y, z + 1)] += sat[I(x, y, z)];

    auto const cubes = [&](vk::i32 x, vk::i32 y, vk::i32 z, vk::i32 ext) noexcept
    {
        auto const lookup = [&](vk::i32 ix, vk::i32 iy, vk::i32 iz) noexcept
        {
            return ix < 0 || iy < 0 || iz < 0
                ? 0
                : sat[I(ix, iy, iz)];
        };
        vk::i32 const x0 = x - ext;
        vk::i32 const x1 = std::min(x + ext - 1, dim[0] - 1);
        vk::i32 const y0 = y - ext;
        vk::i32 const y1 = std::min(y + ext - 1, dim[1] - 1);
        vk::i32 const z0 = z - ext;
        vk::i32 const z1 = std::min(z + ext - 1, dim[2] - 1);
        return lookup(x1, y1, z1)
             - lookup(x1, y1, z0)
             - lookup(x1, y0, z1)
             + lookup(x1, y0, z0)
             - lookup(x0, y1, z1)
             + lookup(x0, y1, z0)
             + lookup(x0, y0, z1)
             - lookup(x0, y0, z0);
    };

    vkQueueWaitIdle(transferQueue);
    {
        vk::guard::MemoryMap<u8> const dist
        ({
            .device = device,
            .memory = stagingMemory,
            .bytes  = voxels,
        });
        auto const anyNear = [&](vk::i32 x, vk::i32 y, vk::i32 z) noexcept
        {
            if(x > 0)
                return dist[I(x - 1, y, z)];
            if(y > 0)
                return dist[I(x, y - 1, z)];
            //if(z > 0)
                return dist[I(x, y, z - 1)];
        };

        dist[0] = 0;
        while(cubes(0, 0, 0, dist[0]) == 0)
            ++dist[0];

        for(vk::i32 z = 0; z < dim[2]; ++z)
        for(vk::i32 y = 0; y < dim[1]; ++y)
        for(vk::i32 x = (y + z == 0 ? 1 : 0); x < dim[0]; ++x)
        {
            vk::u32 const i = I(x, y, z);
            vk::i32 const adj = anyNear(x, y, z);
            vk::i32 const adj_p1 = std::min(254, adj) + 1;
            vk::i32 const adj_m1 = std::max(  1, adj) - 1;
            dist[i] = static_cast<u8>
            (
                cubes(x, y, z, adj_p1) == 0
                    ? adj_p1
                    : (cubes(x, y, z, adj) == 0
                        ? adj
                        : adj_m1)
            );
        }
    }
    copyImage(1);

    vkQueueWaitIdle(transferQueue);
}

void Renderer::render(vk::guard::RenderPass const &pass, RenderData const &data) noexcept
{
    VkPipelineBindPoint const bindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    vkCmdBindPipeline(pass.buffer, bindPoint, graphicsPipeline);
    vkCmdBindDescriptorSets(pass.buffer, bindPoint, graphicsPipelineLayout, 0, 1, &descriptorSet, 0, nullptr);

    auto const R = data.camera.rotate();
    Push const push =
    {
        {
            R[0][0], R[0][1], R[0][2], data.camera.pos[0],
            R[1][0], R[1][1], R[1][2], data.camera.pos[1],
            R[2][0], R[2][1], R[2][2], data.camera.pos[2],
        },
    };
    vkCmdPushConstants(pass.buffer, graphicsPipelineLayout, VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(Push), &push);

    vkCmdDraw(pass.buffer, 6, 1, 0, 0);
}

#pragma once
#include "imgui_state.h"
#include <vk/command_buffer.h>
#include <vk/fence.h>
#include <vk/framebuffer.h>
#include <vk/image_view.h>
#include <vk/guard/command_buffer.h>
#include <vk/guard/render_pass.h>
#include <vk/render_pass.h>
#include <vk/semaphore.h>
#include <vk/swapchain.h>
#include <tracy/Tracy.hpp>
#include <tracy/TracyVulkan.hpp>

struct GPU;

struct Window
{
    VkQueue queue;
    vk::UniqueHandle<GLFWwindow *> window;
    vk::UniqueHandle<VkSurfaceKHR> surface;

    vk::Swapchain swapchain;
    vk::RenderPass renderPass;

    std::vector<VkImage          > image;
    std::vector<vk::ImageView    > imageView;
    std::vector<vk::Framebuffer  > framebuffer;

    std::vector<vk::Semaphore    > waitSemaphore;
    std::vector<vk::Semaphore    > signalSemaphore;
    std::vector<vk::Fence        > inFlightFence;
    std::vector<vk::CommandBuffer> commandBuffer;

    std::vector<vk::UniqueHandle<TracyVkCtx>> tracyContext;

    vk::f32 xscale, yscale;
    vk::u32 width, height;
    VkSurfaceFormatKHR surfaceFormat;

    vk::u32 frame = 0u;
    ImGui_ImplVulkan_InitInfo info;

public:
    Window(GPU const &);

    struct FrameState
    {
        VkDevice        device;
        VkQueue         queue;
        VkSwapchainKHR  swapchain;
        VkCommandBuffer commandBuffer;

        VkSemaphore     signalSemaphore;
        VkSemaphore     waitSemaphore;
        VkFence         inFlightFence;
        TracyVkCtx      tracyContext;

        struct value_t
        {
            VkCommandBuffer commandBuffer;
            vk::u32         imageIndex;
            TracyVkCtx      tracyContext;
        };
        value_t acquire(               ) const;
        void    release(value_t const &) const;
    };

    vk::StoringGuard<FrameState> frameGuard();
    vk::guard::RenderPass renderPassGuard(FrameState::value_t) const;

    bool shouldClose() const noexcept;
    ImguiState imguiState() const noexcept;
};

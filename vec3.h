#pragma once
#include <cmath>

struct vec3
{
    float x, y, z;

    float  operator[](unsigned int const i) const noexcept {return *(&x + i);}
    float &operator[](unsigned int const i)       noexcept {return *(&x + i);}
};

inline vec3 toVec3(float const f) noexcept
{
    return {f, f, f};
}

inline vec3 operator+(vec3 const &v1, vec3 const &v2) noexcept
{
    return {v1.x + v2.x, v1.y + v2.y, v1.z + v2.z};
}
inline vec3 operator-(vec3 const &v1, vec3 const &v2) noexcept
{
    return {v1.x - v2.x, v1.y - v2.y, v1.z - v2.z};
}
inline vec3 operator*(vec3 const &v1, vec3 const &v2) noexcept
{
    return {v1.x * v2.x, v1.y * v2.y, v1.z * v2.z};
}
inline vec3 operator/(vec3 const &v1, vec3 const &v2) noexcept
{
    return {v1.x / v2.x, v1.y / v2.y, v1.z / v2.z};
}
inline vec3 &operator+=(vec3 &v1, vec3 const &v2) noexcept
{
    return v1 = v1 + v2;
}
inline vec3 &operator-=(vec3 &v1, vec3 const &v2) noexcept
{
    return v1 = v1 - v2;
}
inline vec3 &operator*=(vec3 &v1, vec3 const &v2) noexcept
{
    return v1 = v1 * v2;
}
inline vec3 &operator/=(vec3 &v1, vec3 const &v2) noexcept
{
    return v1 = v1 / v2;
}
inline float dot(vec3 const &v1, vec3 const &v2) noexcept
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}
inline float length(vec3 const &v) noexcept
{
    return std::sqrt(dot(v, v));
}
inline vec3 normalize(vec3 const &v) noexcept
{
    return v / toVec3(length(v));
}
inline vec3 cross(vec3 const &v1, vec3 const &v2) noexcept
{
    return
    {
        v1.y * v2.z - v1.z * v2.y,
        v1.z * v2.x - v1.x * v2.z,
        v1.x * v2.y - v1.y * v2.x,
    };
}

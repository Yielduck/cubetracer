#include "gpu.h"
#include "window.h"
#include "renderer.h"
#include "world.h"
#include "gui.h"

#include <GLFW/glfw3.h>
#include <iostream>

int main()
{
    try
    {
        if(GLFW_TRUE != glfwInit())
            throw std::runtime_error("glfwInit: false");
        if(GLFW_TRUE != glfwVulkanSupported())
            throw std::runtime_error("glfwVulkanSupported: false");

        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

        vk::u32 extCount = 0;
        char const * const * const pExt = glfwGetRequiredInstanceExtensions(&extCount);
        if(nullptr == pExt)
            throw std::runtime_error("glfwGetRequiredInstanceExtensions: failed");

        GPU gpu({pExt, extCount});

        Window window(gpu);

        GUI gui(window.imguiState());
        gui.camera.theta = -1.5f;
        gui.camera.pos = {0.f, 100.f, 0.f};

        Renderer renderer(gpu);
        renderer.initPipeline(window.renderPass);
        renderer.update({});

        while(false == window.shouldClose())
        {
            [[maybe_unused]] char const frameName[] = "Frame";
            FrameMarkStart(frameName);

            {
                auto const frame = window.frameGuard();
                Window::FrameState::value_t const &frameInfo = frame;

                vk::guard::CommandBuffer const cmd({frameInfo.commandBuffer, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT});
                TracyVkZone(frameInfo.tracyContext, frameInfo.commandBuffer, "Render");

                {
                    vk::guard::RenderPass const pass = window.renderPassGuard(frameInfo);
                    renderer.render(pass, {gui.camera});
                    gui.render(pass);
                }

                TracyVkCollect(frameInfo.tracyContext, frameInfo.commandBuffer);
            }

            FrameMarkEnd(frameName);
        }
        vkDeviceWaitIdle(gpu.device);
    }
    catch(std::runtime_error const &error)
    {
        std::cerr << error.what() << std::endl;
    }
}

#version 430
layout (location = 0) in vec2 pos;
layout (location = 0) out vec4 color;

uniform layout(binding = 0) usampler3D grid;
uniform layout(binding = 1) usampler3D cubeDistance;

layout (constant_id = 0) const uint ex = 0u;
layout (constant_id = 1) const uint ey = 0u;
layout (constant_id = 2) const uint ez = 0u;
layout (constant_id = 3) const float hitEps = 0.f;

const ivec3 extent = ivec3(ex, ey, ez);
const ivec3 gridMin = -extent / 2;

layout(push_constant) uniform Push
{
    // camera:
    // exx exy exz posx
    // eyx eyy eyz posy
    // ezx ezy ezz posz
    layout(offset = 0) vec4 cam[3];
} push;

vec3 camE   (int i) {return vec3(push.cam[i]);}
vec3 camPos (     ) {return vec3(push.cam[0][3], push.cam[1][3], push.cam[2][3]);}

struct Ray
{
    vec3 o;
    vec3 d;
};
Ray cameraRay(vec2 v)
{
    return Ray
    (
        camPos() - vec3(gridMin),
        normalize(camE(0) * v.x - camE(1) * v.y + camE(2))
    );
}

struct Hit
{
    vec3 pos;
    vec3 norm;
};
const Hit miss =
{
    vec3(0.f),
    vec3(0.f),
};
bool happened(Hit hit)
{
    return hit.pos != vec3(0.f);
}

vec3 outsideHitPos(Hit hit)
{
    return hit.pos + hitEps * hit.norm;
}
vec3 insideHitPos(Hit hit)
{
    return hit.pos - hitEps * hit.norm;
}
ivec3 blockI(Hit hit)
{
    return ivec3(floor(insideHitPos(hit)));
}

Hit closestHit(Ray ray)
{
    ivec3 x = ivec3(floor(ray.o));
    uint dist = texelFetch(cubeDistance, x, 0).r;

    while(true)
    {
        const vec3 t = ((0.5f + (dist - 0.5f) * sign(ray.d)) + x - ray.o) / ray.d;
        const vec3 mask = vec3(lessThanEqual(t.xyz, min(t.yzx, t.zxy)));

        const Hit hit =
        {
            /*.pos  = */ ray.o + ray.d * dot(t, mask),
            /*.norm = */ -sign(ray.d) * mask,
        };

        x = blockI(hit);
        if(lessThan(x, extent) != lessThanEqual(ivec3(0), x))
            break;

        dist = texelFetch(cubeDistance, x, 0).r;
        if(dist == 0)
            return hit;

        ray.o = insideHitPos(hit);
    }
    return miss;
}

vec3 trace(Ray ray)
{
    const vec3 albedo[3] =
    {
        vec3(0.f, 0.f, 0.f),
        vec3(0.4f, 0.4f, 0.4f),
        vec3(0.50f, 0.80f, 0.20f),
    };
    const vec3   skyColor = vec3(0.24f, 0.41f, 0.92f);
    const vec3 lightColor = vec3(1.00f, 0.98f, 0.88f);
    //const vec3 lightDir   = normalize(vec3(2.f, 4.f, 1.f));
    const vec3 lightPos = extent / 2 + vec3(0.f, 50.f, 0.f);

    Hit hit = closestHit(ray);
    vec3 hitPos = outsideHitPos(hit);
    if(!happened(hit))
        return skyColor;

    vec3 l = lightPos - hitPos;
    vec3 lightDir = normalize(l);
    float I = 5000.f / dot(l, l);
    uint type = texelFetch(grid, blockI(hit), 0).r;

    Ray shadowRay = Ray(hitPos, lightDir);
    vec3 color = dot(hit.norm, lightDir) < 0 || happened(closestHit(shadowRay))
        ? vec3(0.f)
        : lightColor * I * dot(hit.norm, lightDir);
    return (skyColor / 3.1416f + color) * albedo[type];
}

vec3 expose(vec3 color, float exposure)
{
    return vec3(1.f) - exp(color * (-exposure));
}
void main()
{
    vec2 d = 0.25f * vec2(dFdx(pos).x, dFdy(pos).y);
    Ray ray[4] =
    {
        cameraRay(pos + vec2( d.x, -d.y)),
        cameraRay(pos + vec2( d.x,  d.y)),
        cameraRay(pos + vec2(-d.x, -d.y)),
        cameraRay(pos + vec2(-d.x,  d.y)),
    };
    vec3 c = vec3(0.f);
    for(int i = 0; i < 4; ++i)
        c += 0.25f * trace(ray[i]);
    //vec3 c = trace(cameraRay(pos));
    c = expose(c, 2.5f);
    color = vec4(c, 1.f);
}

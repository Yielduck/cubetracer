#version 440
layout (location = 0) out vec2 pos;
void main()
{
    vec2 p[4] =
    {
        vec2(-1.f, -1.f),
        vec2(-1.f,  1.f),
        vec2( 1.f, -1.f),
        vec2( 1.f,  1.f),
    };
    int i = gl_VertexIndex < 3
        ? gl_VertexIndex
        : gl_VertexIndex - 2;
    pos = p[i];
    gl_Position = vec4(pos, 0.f, 1.f);
}

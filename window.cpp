#include "window.h"
#include "gpu.h"
#include <GLFW/glfw3.h>
#include <vk/image.h>
#include <vk/proc_addr.h>
#include <vk/queue_present.h>
#include <vk/queue_submit.h>
#include <stdexcept>
#include <ranges>

Window::Window(GPU const &gpu)
    : queue(gpu.graphicsQueue.handle)
{
    GLFWmonitor * const monitor = glfwGetPrimaryMonitor();
    GLFWvidmode const * const mode = glfwGetVideoMode(monitor);

    glfwGetMonitorContentScale(monitor, &xscale, &yscale);

    GLFWwindow * const pWindow = glfwCreateWindow(mode->width, mode->height, "The Window", monitor, NULL);
    if(nullptr == pWindow)
        throw std::runtime_error("glfwCreateWindow: could not create window");
    window = vk::UniqueHandle<GLFWwindow *>(pWindow, glfwDestroyWindow);

    VkSurfaceKHR surfaceKHR = VK_NULL_HANDLE;
    if(VK_SUCCESS != glfwCreateWindowSurface(gpu.instance, window, nullptr, &surfaceKHR))
        throw std::runtime_error("glfwCreateWindowSurface: could not create window surface");
    surface = vk::UniqueHandle<VkSurfaceKHR>
    (
        surfaceKHR,
        [instance = gpu.instance.get()](VkSurfaceKHR const handle) noexcept
        {
            vkDestroySurfaceKHR(instance, handle, nullptr);
        }
    );

    VkBool32 result;
    vkGetPhysicalDeviceSurfaceSupportKHR(gpu.deviceInfo.device, gpu.graphicsQueue.family, surface, &result);
    if(VK_TRUE != result)
        throw std::runtime_error("vkGetPhysicalDeviceSurfaceSupportKHR: failure");

    {
        int fbw = 0;
        int fbh = 0;
        glfwGetFramebufferSize(window, &fbw, &fbh);
        width  = static_cast<vk::u32>(fbw);
        height = static_cast<vk::u32>(fbh);
    }

    VkSurfaceFormatKHR fmt[2];
    vk::u32 formatCount = 2u;
    vkGetPhysicalDeviceSurfaceFormatsKHR(gpu.deviceInfo.device, surface, &formatCount, fmt);
    surfaceFormat = static_cast<int>(fmt[0].format) % 7 == 1 ? fmt[1] : fmt[0]; // prefer non-srgb

    VkSurfaceCapabilitiesKHR capabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(gpu.deviceInfo.device, surface, &capabilities);

    swapchain = vk::createSwapchain
    (
        gpu.device,
        {
            .surface = surface,
            .minImageCount = capabilities.minImageCount,
            .surfaceFormat = surfaceFormat,
            .extent = {width, height},
        }
    );

    vk::u32 imageCount = 0u;
    vkGetSwapchainImagesKHR(gpu.device, swapchain, &imageCount, nullptr);
    image.resize(imageCount);
    vkGetSwapchainImagesKHR(gpu.device, swapchain, &imageCount, image.data());

    imageView.reserve(imageCount);
    for(VkImage const handle : image)
        imageView.push_back
        (
            vk::createImageView
            (
                gpu.device,
                {
                    .image = handle,
                    .type = VK_IMAGE_VIEW_TYPE_2D,
                    .format = surfaceFormat.format,
                }
            )
        );

    renderPass = vk::createRenderPass
    (
        gpu.device,
        {
            .attachment =
            {{ {
                .flags          = {},
                .format         = surfaceFormat.format,
                .samples        = VK_SAMPLE_COUNT_1_BIT,
                .loadOp         = VK_ATTACHMENT_LOAD_OP_CLEAR,
                .storeOp        = VK_ATTACHMENT_STORE_OP_STORE,
                .stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                .initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED,
                .finalLayout    = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            }} },
            .subpass =
            {{ {
                .colorAttachment =
                {{ {
                      .attachment = 0u,
                      .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                }} }
            }} },
            .dependency =
            {{ {
                .srcSubpass = VK_SUBPASS_EXTERNAL,
                .dstSubpass = 0u,
                .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                .srcAccessMask = 0,
                .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                .dependencyFlags = {},
            }} }
        }
    );

    framebuffer.reserve(imageCount);
    for(vk::u32 const i : std::views::iota(0u, imageCount))
        framebuffer.push_back
        (
            vk::createFramebuffer
            (
                gpu.device,
                {
                    .renderPass = renderPass,
                    .attachment = {{imageView[i]}},
                    .width = width,
                    .height = height,
                }
            )
        );
    for([[maybe_unused]] vk::u32 const i : std::views::iota(0u, imageCount))
    {
          waitSemaphore.push_back(vk::createSemaphore(gpu.device));
        signalSemaphore.push_back(vk::createSemaphore(gpu.device));
        inFlightFence.push_back(vk::createFence(gpu.device, VK_FENCE_CREATE_SIGNALED_BIT));
        commandBuffer.push_back(vk::createCommandBuffer(gpu.device, {.commandPool = gpu.graphicsQueue.commandPool}));

        TracyVkCtx const context = TracyVkContextCalibrated
        (
            gpu.deviceInfo.device,
            gpu.device,
            queue,
            commandBuffer.back(),
            instanceProcAddr(gpu.instance, vkGetPhysicalDeviceCalibrateableTimeDomainsEXT),
            deviceProcAddr(gpu.device, vkGetCalibratedTimestampsEXT)
        );
        tracyContext.emplace_back
        (
            context,
            +[]([[maybe_unused]] TracyVkCtx const ctx) noexcept {TracyVkDestroy(ctx);}
        );
    }

    info =
    {
        .Instance       = gpu.instance,
        .PhysicalDevice = gpu.deviceInfo.device,
        .Device         = gpu.device,
        .QueueFamily    = gpu.graphicsQueue.family,
        .Queue          = gpu.graphicsQueue.handle,
        .PipelineCache  = VK_NULL_HANDLE,
        .DescriptorPool = gpu.imguiDescriptorPool,
        .Subpass        = 0u,
        .MinImageCount  = capabilities.minImageCount,
        .ImageCount     = imageCount,
        .MSAASamples    = VK_SAMPLE_COUNT_1_BIT,
        .Allocator      = vk::pAlloc,
        .CheckVkResultFn = +[](VkResult const vkresult)
        {
            if(VK_SUCCESS != vkresult)
                throw std::runtime_error("imgui: something went wrong");
        }
    };
}

bool Window::shouldClose() const noexcept
{
    return glfwWindowShouldClose(window);
}
ImguiState Window::imguiState() const noexcept
{
    return
    {
        .window = window,
        .info = info,
        .renderPass = renderPass,
        .commandBuffer = commandBuffer[0],
        .fontScale = std::min(xscale, yscale),
    };
}

vk::guard::RenderPass Window::renderPassGuard(FrameState::value_t const frameInfo) const
{
    VkRect2D const renderArea =
    {
        {0, 0},
        {width, height},
    };
    VkViewport const viewport =
    {
        .x        = 0.f,
        .y        = 0.f,
        .width    = static_cast<vk::f32>(width),
        .height   = static_cast<vk::f32>(height),
        .minDepth = 0.f,
        .maxDepth = 1.f,
    };

    vk::guard::RenderPass guard
    ({
        .commandBuffer  = frameInfo.commandBuffer,
        .renderPass     = renderPass,
        .framebuffer    = framebuffer[frameInfo.imageIndex],
        .renderArea     = renderArea,
        .clearValue     = {{ {{{0.f, 0.f, 0.f, 1.f}}} }},
    });

    vkCmdSetViewport(frameInfo.commandBuffer, 0, 1, &viewport);
    vkCmdSetScissor (frameInfo.commandBuffer, 0, 1, &renderArea);

    return guard;
}

vk::StoringGuard<Window::FrameState> Window::frameGuard()
{
    vk::StoringGuard<FrameState> guard
    ({
        .device             = info.Device,
        .queue              = info.Queue,
        .swapchain          = swapchain,
        .commandBuffer      =   commandBuffer[frame],
        .signalSemaphore    = signalSemaphore[frame],
        .waitSemaphore      =   waitSemaphore[frame],
        .inFlightFence      =   inFlightFence[frame],
        .tracyContext       =    tracyContext[frame],
    });
    frame = (frame + 1) % info.ImageCount;
    return guard;
}
Window::FrameState::value_t Window::FrameState::acquire() const
{
    {
        ZoneScopedNC("glfwPollEvents", 0x010101);
        glfwPollEvents();
    }
    {
        ZoneScopedNC("vkQueueSubmit fence wait", 0xa00000);
        vk::waitForFences(device, {{inFlightFence}});
    }

    vk::u32 imageIndex = 0u;
    vkAcquireNextImageKHR(device, swapchain, UINT64_MAX, waitSemaphore, VK_NULL_HANDLE, &imageIndex);

    return
    {
        .commandBuffer  = commandBuffer,
        .imageIndex     = imageIndex,
        .tracyContext   = tracyContext,
    };
}
void Window::FrameState::release(value_t const &info) const
{
    vk::resetFences(device, {{inFlightFence}});
    {
        ZoneScopedNC("vkAcquireNextImage semaphore wait", 0x00a000);
        vk::queueSubmit
        (
            queue,
            {{
                {
                    .waitSemaphore      = {{waitSemaphore}},
                    .waitDstStageMask   = {{VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT}},
                    .commandBuffer      = {{commandBuffer}},
                    .signalSemaphore    = {{signalSemaphore}},
                }
            }},
            inFlightFence
        );
    }
    vk::queuePresent
    (
        queue,
        {
            .waitSemaphore  = {{signalSemaphore}},
            .swapchain      = swapchain,
            .imageIndex     = info.imageIndex,
        }
    );
}

#pragma once
#include "vec3.h"
#include <array>
struct Camera
{
    float phi, theta;
    float ratio, tanFOV;
    vec3 pos;

    std::array<vec3, 3> rotate() const noexcept
    {
        vec3 const z =
        {
            std::cos(theta) * std::sin(phi),
            std::sin(theta),
            std::cos(theta) * std::cos(phi),
        };
        vec3 const x0 = normalize(cross({0.f, 1.f, 0.f}, z));
        vec3 const y0 = cross(z, x0);
        vec3 const x = x0 * toVec3(tanFOV * ratio);
        vec3 const y = y0 * toVec3(tanFOV);
        return {x, y, z};
    }
};

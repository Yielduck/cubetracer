#pragma once
#include <vk/command_pool.h>
#include <vk/descriptor_pool.h>
#include <vk/device.h>
#include <vk/instance.h>
#include <vk/physical_device_info.h>
struct GPU
{
    vk::Instance instance;

    vk::PhysicalDeviceInfo deviceInfo;
    vk::Device device;

    vk::DescriptorPool descriptorPool;
    vk::DescriptorPool imguiDescriptorPool;

    struct Queue
    {
        vk::u32 family;
        VkQueue handle;
        vk::CommandPool commandPool;
    };
    Queue graphicsQueue;
    Queue computeQueue;
    Queue transferQueue;

    GPU(vk::cspan<char const *> instanceExt = {});
};
